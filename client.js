// Définition des éléments HTML qui nous seront utiles
const RESOURCE_INPUT = document.getElementById("resource");
const ID_INPUT = document.getElementById("id");
const GET_BUTTON = document.getElementById("get");
const TITLE_INPUT = document.getElementById("title");
const CONTENT_INPUT = document.getElementById("content");
const POST_BUTTON = document.getElementById("post");
const PUT_BUTTONS = document.querySelectorAll("[value='PUT']");
const DELETE_BUTTONS = document.querySelectorAll("[value='DELETE']");
const RESPONSE_AREA = document.getElementById("response");

// Fonction qui supprime tous les noeuds enfants
// TODO probablement plus optimisé à faire
function removeChilds(parent) {
  while (parent.lastChild) {
    parent.removeChild(parent.lastChild);
  }
};

// Fonction qui execute une requete et écrit son resultat dans la page
async function query(resource, method, body = {}) {
  body = JSON.stringify(body);

  console.log("Querying:", resource, ", with method:", method, ", with body content:", body);

  const response = await fetch(URL + "v" + VERSION + "/" + resource, {
    // mode: "no-cors", // WARNING ce truc est problématique
    method: method, // POST, *GET, PATCH, PUT, DELETE
    headers: {
      "Content-Type":
        method == "POST" || method == "PUT"
          ? "application/json"
          : "application/x-www-form-urlencoded; charset=UTF-8",
    },
    body: body.length > 2 ? body.length : null, // must match "Content-Type" header
  })
    .then(response => {
      console.log("Received:", response)
      return response.json()
    })
    .catch(error => {
      // Traitement en cas d’erreur
      console.error("Problem with the fetch operation:", error)
    });

  // Traitement lorsque la réponse a été reçue
  removeChilds(RESPONSE_AREA);
  for (let i = 0; i < response.data.length; i++) {
    const article_data = response.data[i];

    let article = document.createElement("article");
    article.id = article_data.id;

    let h2 = document.createElement("h3");
    h2.appendChild(
      document.createTextNode(article_data.title)
    );

    let p_infos = document.createElement("details");
    let p_infos_sum = document.createElement("summary");
    p_infos_sum.appendChild(
      document.createTextNode(
        `Publié le ${article_data.publication_date}`
      )
    );
    p_infos.appendChild(p_infos_sum);

    let p_content = document.createElement("p");
    p_content.appendChild(
      document.createTextNode(article_data.content)
    );

    let actions = document.createElement("footer");
    actions.innerHTML = `
      <button type="submit" name="submitter" value="PUT">
        Modifier
      </button>
      <button type="submit" name="submitter" value="DELETE">
        Supprimer
      </button>
    `;

    article.appendChild(h2);
    article.appendChild(p_infos);
    article.appendChild(p_content);
    article.appendChild(actions);

    RESPONSE_AREA.appendChild(article);
  }
}

// Execution d’une requête GET initiale au chargement de la page
query("articles", "GET");

// Réactions aux évennements des boutons d’action
POST_BUTTON.addEventListener("click", (event) => {
  let user = 1; // TODO be able to post as any user
  query(RESOURCE_INPUT.value, "POST", { "title": TITLE_INPUT.value, "content": CONTENT_INPUT.value, "user": user });
});

GET_BUTTON.addEventListener("click", (event) => {
  query(RESOURCE_INPUT.value + "/" + ID_INPUT.value, "GET");
});

for (const button of PUT_BUTTONS) {
  button.addEventListener("click", (event) => {
    let id = 5; // TODO get the right ID
    query(RESOURCE_INPUT.value + "/" + id, "PUT", { "title": TITLE_INPUT.value, "content": CONTENT_INPUT.value });
  });
}

for (const button of DELETE_BUTTONS) {
  button.addEventListener("click", (event) => {
    let id = 5; // TODO get the right ID
    query(RESOURCE_INPUT.value + "/" + id, "DELETE");
  });
}
